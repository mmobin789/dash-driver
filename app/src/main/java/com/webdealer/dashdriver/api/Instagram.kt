package com.webdealer.dashdriver.api

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.graphics.Bitmap
import android.net.Uri
import android.os.Parcelable
import android.util.Log
import android.view.ViewGroup
import android.view.Window
import android.webkit.WebResourceError
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.LinearLayout
import android.widget.Toast
import com.google.gson.annotations.SerializedName
import com.webdealer.dashdriver.contracts.OnInstagramAccessListener
import kotlinx.android.parcel.Parcelize

data class Instagram(
        val data: InstagramUser) {
    @Parcelize
    data class InstagramUser(val id: String, val username: String, @field:SerializedName("full_name") val fullName: String,
                             @field:SerializedName("profile_picture") val profilePic: String,
                             val bio: String
    ) : Parcelable

    companion object {

        @SuppressLint("SetJavaScriptEnabled")
        fun loginInstagram(context: Context, onInstagramAccessListener: OnInstagramAccessListener) {
            val dialog = Dialog(context)
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            val ui = LinearLayout(context)
            ui.layoutParams = LinearLayout.LayoutParams(ViewGroup.MarginLayoutParams.MATCH_PARENT, ViewGroup.MarginLayoutParams.MATCH_PARENT)
            ui.orientation = LinearLayout.VERTICAL
            val webView = WebView(context)
            webView.layoutParams = LinearLayout.LayoutParams(ViewGroup.MarginLayoutParams.WRAP_CONTENT, ViewGroup.MarginLayoutParams.WRAP_CONTENT)
            webView.settings.javaScriptEnabled = true
            ui.addView(webView)
            dialog.setContentView(ui)
            webView.loadUrl(Instagram.insta_auth_url)
            webView.webViewClient = object : WebViewClient() {
                private var accessToken = ""

                override fun onReceivedError(view: WebView?, request: WebResourceRequest?, error: WebResourceError?) {
                    Log.e("Instagram Error", error.toString())
                }

                override fun onPageStarted(view: WebView, url: String, favicon: Bitmap?) {
                    Log.d("Insta_url", url)
                    if (url.contains("#access_token=")) {
                        val uri = Uri.parse(url)
                        accessToken = uri.encodedFragment
                        accessToken = accessToken.substring(accessToken.lastIndexOf("=") + 1)
                        Log.i("InstagramToken", accessToken)
                        onInstagramAccessListener.onInstagramAccessGranted(accessToken)
                        dialog.dismiss()

                    } else if (url.contains("?error")) {
                        Toast.makeText(context, "Error Occured", Toast.LENGTH_SHORT).show()
                        dialog.dismiss()
                    }
                }
            }
            dialog.show()


        }


        const val insta_BASE_URL = "https://api.instagram.com/"
        private const val insta_auth_url = "${insta_BASE_URL}oauth/authorize/?client_id=18c937540d3d490099c94161bd59417c&redirect_uri=https://www.stacktex.com/&response_type=token&scope=public_content"
    }
}