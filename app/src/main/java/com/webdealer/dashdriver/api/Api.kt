package com.webdealer.dashdriver.api

import android.content.Context
import android.util.Log
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import com.webdealer.dashdriver.models.ApiResponse
import com.webdealer.dashdriver.utils.AppStorage
import com.webdealer.dashdriver.utils.Utils
import io.reactivex.Observable
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.OkHttpClient
import okhttp3.RequestBody
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.io.File
import java.util.concurrent.TimeUnit

object Api {

    private const val baseUrl = "http://52.35.158.11/dashweb/web/index.php/releaser/"
    private var retrofit: Retrofit? = null

    fun getWebApi(): WebService {
        if (retrofit == null) {
            val loggingInterceptor = HttpLoggingInterceptor()
            loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
            val httpClient = OkHttpClient.Builder().addInterceptor(loggingInterceptor)
                    .connectTimeout(30, TimeUnit.SECONDS)// connect timeout
                    .readTimeout(30, TimeUnit.SECONDS)    // socket timeout
            retrofit = Retrofit.Builder().baseUrl(baseUrl)
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(httpClient.build())
                    .build()
        }

        return retrofit!!.create(WebService::class.java)
    }

    private fun getImageRequestBody(context: Context, file: File, key: String): MultipartBody.Part {
        val compressed = Utils.getCompressedFile(context, file)
        Log.d("compressedImage", compressed.path)
        val mFile = RequestBody.create(MediaType.parse("image/*"), compressed)
        return MultipartBody.Part.createFormData(key, compressed.name, mFile)
    }

    private fun getTextRequestBody(data: String): RequestBody {
        return RequestBody.create(MediaType.parse("text/plain"), data)
    }

    fun uploadDocs(context: Context, carType: String, license: File, vehicle: File, profilePicture: File, carPicture: File, insuranceDocument: File, data: String): Observable<ApiResponse> {
        return getWebApi().driverDocs(getTextRequestBody(AppStorage.getUser().email), getTextRequestBody(data), getImageRequestBody(context, license, "license"),
                getImageRequestBody(context, vehicle, "vehreg"), getTextRequestBody(carType), getImageRequestBody(context, insuranceDocument, "insuranceDoc"), getImageRequestBody(context, profilePicture, "driverPicture"), getImageRequestBody(context, carPicture, "carPicture"))
    }
}