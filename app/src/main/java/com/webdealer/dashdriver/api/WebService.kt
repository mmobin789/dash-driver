package com.webdealer.dashdriver.api

import com.webdealer.dashdriver.models.ApiResponse
import com.webdealer.dashdriver.models.Auth
import com.webdealer.dashdriver.models.DriverInfo
import io.reactivex.Observable
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.http.*

interface WebService {
    @POST("login")
    fun login(@Body auth: Auth): Observable<ApiResponse>

    @Multipart
    @POST("driverdocs")
    fun driverDocs(@Part("email") email: RequestBody, @Part("driverInfo") driverBackgroundInfo: RequestBody, @Part license: MultipartBody.Part, @Part vehreg: MultipartBody.Part
                   , @Part("CarType") carType: RequestBody, @Part insuranceDoc: MultipartBody.Part, @Part profilePic: MultipartBody.Part,
                   @Part carPic: MultipartBody.Part): Observable<ApiResponse>

    @POST("register")
    fun register(@Body auth: Auth): Observable<ApiResponse>

    @POST("verify")
    fun phoneVerify(@Body auth: Auth): Observable<ApiResponse>


    @POST("resendcode")
    fun resendCode(@Body auth: Auth): Observable<ApiResponse>

    @POST("fcmtoken")
    fun fcm(@Body auth: Auth): Observable<ApiResponse>

    @POST("getdriverinfo")
    fun getDriverInfo(@Body auth: Auth): Observable<ApiResponse>

    @POST("acceptride")
    fun acceptRide(@Body auth: Auth): Observable<ApiResponse>

    @POST("cancelride")
    fun cancelRide(@Body auth: Auth): Observable<ApiResponse>

    @POST("confirmarrival")
    fun confirmArrival(@Body auth: Auth): Observable<ApiResponse>

    @POST("startride")
    fun startRide(@Body auth: Auth): Observable<ApiResponse>

    @POST("endride")
    fun endRide(@Body auth: Auth): Observable<ApiResponse>

    @POST("ridecomplete")
    fun rideComplete(@Body auth: Auth): Observable<ApiResponse>

    @POST("trackdriver")
    fun realTimeTrackDriver(@Body driverInfo: DriverInfo): Observable<ApiResponse>

    @POST("setdriverstatus")
    fun setDriverStatus(@Body auth: Auth): Observable<ApiResponse>

    @POST("getriderinfo")
    fun getRiderInfo(@Query("rideID") rideID: String): Observable<ApiResponse>

    @GET("${Instagram.insta_BASE_URL}v1/users/self")
    fun getInstagramUser(@Query("access_token") accessToken: String): Observable<Instagram>

}