package com.webdealer.dashdriver.services

import android.Manifest
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.arch.lifecycle.LifecycleService
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.media.RingtoneManager
import android.os.Build
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.support.v4.app.ActivityCompat
import android.support.v4.app.NotificationCompat
import android.support.v4.content.ContextCompat
import android.util.Log
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.LocationCallback
import com.google.android.gms.location.LocationResult
import com.google.android.gms.location.LocationServices
import com.webdealer.dashdriver.R
import com.webdealer.dashdriver.contracts.OnApiListener
import com.webdealer.dashdriver.models.ApiResponse
import com.webdealer.dashdriver.models.DriverInfo
import com.webdealer.dashdriver.ui.activities.Base
import com.webdealer.dashdriver.ui.activities.Base.Companion.dashDriverImpl
import com.webdealer.dashdriver.ui.activities.MainActivity
import com.webdealer.dashdriver.utils.AppStorage


class DriverLocationSync : LifecycleService(), GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {


    private val locationDelay: Long = 10 // seconds
    // private var orderID = ""
    // private var socket: Socket? = null


    override fun onConnectionFailed(p0: ConnectionResult) {

    }

//    private fun initSocket(): Socket? {
//        try {
//            val socket = IO.socket("http://goldback.business/api/latlong")
//            socket.connect()
//            return socket
//
//        } catch (e: URISyntaxException) {
//            e.printStackTrace()
//        }
//        return null
//
//    }

    override fun onCreate() {
        super.onCreate()

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            startForegroundWithNotification()
        }

    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun startForegroundWithNotification() {
        val intent = Intent(this, MainActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        val pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT)
        val channelName = "Dash Driver Sync"
        val channelId = "$packageName.ChannelID"

        val channel = NotificationChannel(channelId, channelName, NotificationManager.IMPORTANCE_HIGH)
        channel.enableLights(true)
        channel.lightColor = ContextCompat.getColor(this, R.color.colorAccent)
        val manager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        manager.createNotificationChannel(channel)


        val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        val notificationBuilder = NotificationCompat.Builder(this, channelId)
                //.setSmallIcon(R.drawable.arrowicon)
                .setContentTitle(getString(R.string.app_name))
                .setContentText("Driver Location Sync")
                .setAutoCancel(false)
                .setSound(defaultSoundUri)

                .setContentIntent(pendingIntent)
        startForeground(33, notificationBuilder.build())
    }

    override fun onConnected(p0: Bundle?) {
        // socket = initSocket()
        startLocationUpdate()
    }

    private fun startLocationUpdate() {

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {


            LocationServices.getFusedLocationProviderClient(this).requestLocationUpdates(Base.getLocationRequest(locationDelay), locationCallback, null)
        }
    }

    private fun stopLocationUpdate() {
        LocationServices.getFusedLocationProviderClient(this).removeLocationUpdates(locationCallback)

    }


    override fun onConnectionSuspended(p0: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }


    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        //    if (intent != null)
        //  orderID = intent.getStringExtra("orderID")
        super.onStartCommand(intent, flags, startId)
        Base.buildGoogleApiClient(this, this, this).connect()

        return START_REDELIVER_INTENT
    }


    private fun sendLocation(location: LocationResult) {

        val lat = location.lastLocation.latitude
        val lng = location.lastLocation.longitude
        Log.v("LocationSync", lat.toString() + "/" + lng)
        Log.i("locationDelay", "After $locationDelay seconds")
        AppStorage.init(this)
        if (AppStorage.getUser() != null) {
            val driverInfo = DriverInfo("", "")
            driverInfo.currentlat = lat.toString()
            driverInfo.currentlng = lng.toString()
            dashDriverImpl.realTimeDriverLocation(this, driverInfo, object : OnApiListener {
                override fun onApiResponse(apiResponse: ApiResponse) {
                    if (apiResponse.status)
                        Log.i(javaClass.simpleName, apiResponse.message)
                }

                override fun onApiError(error: String) {

                }
            })
        }

//        socket!!.emit(data).on(data, {
//            val obj = it[0] as JSONObject
//            Log.i("DriverLocationAPI", obj.toString())
//
//        })

    }


    //   private var location: LocationResult? = null
    private val locationCallback = object : LocationCallback() {

        override fun onLocationResult(locationResult: LocationResult?) {

            sendLocation(locationResult!!)
        }
    }

    override fun onDestroy() {
        stopLocationUpdate()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
            stopForeground(true)
        super.onDestroy()
        // socket!!.disconnect()
    }

//    inner class LocationBinder : Binder() {
//        //   fun getService(): LocationSync = this@LocationSync
//    }

}
