package com.webdealer.dashdriver.services

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.media.RingtoneManager
import android.os.Build
import android.support.v4.app.NotificationCompat
import android.util.Log
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.webdealer.dashdriver.R
import com.webdealer.dashdriver.ui.activities.MainActivity
import com.webdealer.dashdriver.utils.AppStorage

class Fcm : FirebaseMessagingService() {
    override fun onNewToken(p0: String?) {
        Log.d(javaClass.simpleName, "Refreshed token: " + p0!!)
        AppStorage.setFCM(p0)

    }

    private fun getActivity(remoteMessage: RemoteMessage): Intent {
        val main = Intent(this, MainActivity::class.java)
        main.putExtra("rideID", remoteMessage.data["rideID"])
        return main
    }

    override fun onMessageReceived(p0: RemoteMessage) {
        sendNotification(p0)
    }

    private fun sendNotification(remoteMessage: RemoteMessage) {

        val intent = getActivity(remoteMessage)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        val pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT)

        val channelId = "111"
        val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        val notificationBuilder = NotificationCompat.Builder(this, channelId)
                //   .setSmallIcon(R.drawable.arrowicon)
                .setContentTitle(getString(R.string.app_name))
                .setContentText(remoteMessage.notification!!.body)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent)

        val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(channelId,
                    "Channel human readable title",
                    NotificationManager.IMPORTANCE_DEFAULT)
            notificationManager.createNotificationChannel(channel)
        }

        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build())
    }

}