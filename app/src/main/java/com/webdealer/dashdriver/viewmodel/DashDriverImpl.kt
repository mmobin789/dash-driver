package com.webdealer.dashdriver.viewmodel

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModel
import android.util.Log
import com.webdealer.dashdriver.api.Api
import com.webdealer.dashdriver.api.Api.getWebApi
import com.webdealer.dashdriver.api.Instagram
import com.webdealer.dashdriver.contracts.OnApiListener
import com.webdealer.dashdriver.contracts.OnInstagramLoginListener
import com.webdealer.dashdriver.models.ApiResponse
import com.webdealer.dashdriver.models.Auth
import com.webdealer.dashdriver.models.DriverBackgroundInfo
import com.webdealer.dashdriver.models.DriverInfo
import com.webdealer.dashdriver.services.DriverLocationSync
import com.webdealer.dashdriver.ui.activities.Base
import com.webdealer.dashdriver.ui.activities.Base.Companion.fcmData
import com.webdealer.dashdriver.ui.fragments.BaseUI
import com.webdealer.dashdriver.utils.AppStorage
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import java.io.File

class DashDriverImpl : ViewModel() {

    private lateinit var disposable: Disposable

    private fun getBasicAuth(): Auth {
        val auth = Auth(AppStorage.getUser().email, "")
        auth.rideID = if (fcmData?.getString("rideID") != null) {
            val rideID = fcmData!!.getString("rideID")
            Log.i("rideID", rideID)
            rideID
        } else {
            "N/A"
        }

        return auth
    }

    fun getInstagramUser(base: Base, accessToken: String, onInstagramLoginListener: OnInstagramLoginListener) {
        disposable = getWebApi().getInstagramUser(accessToken).observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io())
                .subscribe(
                        { instagram ->
                            val apiResponse = MutableLiveData<Instagram>()
                            apiResponse.postValue(instagram)
                            apiResponse.observe(base, Observer {

                                onInstagramLoginListener.onSuccess(it!!)
                                disposable.dispose()
                            })
                        }, {
                    it.printStackTrace()

                })
    }

    fun login(base: Base, email: String, password: String, onApiListener: OnApiListener) {
        val data = Auth(email, password)
        disposable = getWebApi().login(data).observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io())
                .subscribe(
                        {
                            val apiResponse = MutableLiveData<ApiResponse>()
                            apiResponse.postValue(it)
                            apiResponse.observe(base, Observer {

                                onApiListener.onApiResponse(it!!)
                                disposable.dispose()
                            })
                        }, {
                    onApiListener.onApiError(it.toString())
                })
    }

    fun register(base: Base, auth: Auth, onApiListener: OnApiListener) {

        disposable = getWebApi().register(auth).observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io())
                .subscribe(
                        {
                            val apiResponse = MutableLiveData<ApiResponse>()
                            apiResponse.postValue(it)
                            apiResponse.observe(base, Observer {

                                onApiListener.onApiResponse(it!!)
                                disposable.dispose()
                            })
                        }, {
                    onApiListener.onApiError(it.toString())
                })
    }

    fun verifyPhone(base: BaseUI, auth: Auth, onApiListener: OnApiListener) {

        disposable = getWebApi().phoneVerify(auth).observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io())
                .subscribe(
                        {
                            val apiResponse = MutableLiveData<ApiResponse>()
                            apiResponse.postValue(it)
                            apiResponse.observe(base, Observer {

                                onApiListener.onApiResponse(it!!)
                                disposable.dispose()
                            })
                        }, {
                    onApiListener.onApiError(it.toString())
                })
    }

    fun resendCode(base: BaseUI, auth: Auth, onApiListener: OnApiListener) {

        disposable = getWebApi().resendCode(auth).observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io())
                .subscribe(
                        {
                            val apiResponse = MutableLiveData<ApiResponse>()
                            apiResponse.postValue(it)
                            apiResponse.observe(base, Observer {

                                onApiListener.onApiResponse(it!!)
                                disposable.dispose()
                            })
                        }, {
                    onApiListener.onApiError(it.toString())
                })
    }

    fun fcm(base: Base, auth: Auth, onApiListener: OnApiListener) {

        disposable = getWebApi().fcm(auth).observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io())
                .subscribe(
                        {
                            val apiResponse = MutableLiveData<ApiResponse>()
                            apiResponse.postValue(it)
                            apiResponse.observe(base, Observer {

                                onApiListener.onApiResponse(it!!)
                                disposable.dispose()
                            })
                        }, {
                    onApiListener.onApiError(it.toString())
                })
    }

    fun driverDocs(baseUI: BaseUI, carType: String, license: File, veh: File, profilePicture: File, carPicture: File, insuranceDocument: File, driverBackgroundInfo: DriverBackgroundInfo, onApiListener: OnApiListener) {

        disposable = Api.uploadDocs(baseUI.context!!, carType, license, veh, profilePicture, carPicture, insuranceDocument, driverBackgroundInfo.toJSON()).observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io())
                .subscribe(
                        { api ->
                            val apiResponse = MutableLiveData<ApiResponse>()
                            apiResponse.postValue(api)
                            apiResponse.observe(baseUI, Observer {

                                onApiListener.onApiResponse(it!!)
                                disposable.dispose()
                            })
                        }, {
                    onApiListener.onApiError(it.toString())
                })
    }


    fun acceptRide(baseUI: BaseUI, onApiListener: OnApiListener) {

        disposable = getWebApi().acceptRide(getBasicAuth()).observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io())
                .subscribe(
                        {
                            val apiResponse = MutableLiveData<ApiResponse>()
                            apiResponse.postValue(it)
                            apiResponse.observe(baseUI, Observer {

                                onApiListener.onApiResponse(it!!)
                                disposable.dispose()
                            })
                        }, {
                    onApiListener.onApiError(it.toString())
                })
    }

    fun cancelRide(baseUI: BaseUI, onApiListener: OnApiListener) {

        disposable = getWebApi().cancelRide(getBasicAuth()).observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io())
                .subscribe(
                        {
                            val apiResponse = MutableLiveData<ApiResponse>()
                            apiResponse.postValue(it)
                            apiResponse.observe(baseUI, Observer {

                                onApiListener.onApiResponse(it!!)
                                disposable.dispose()
                            })
                        }, {
                    onApiListener.onApiError(it.toString())
                })
    }


    fun confirmArrival(baseUI: BaseUI, onApiListener: OnApiListener) {

        disposable = getWebApi().confirmArrival(getBasicAuth()).observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io())
                .subscribe(
                        {
                            val apiResponse = MutableLiveData<ApiResponse>()
                            apiResponse.postValue(it)
                            apiResponse.observe(baseUI, Observer {

                                onApiListener.onApiResponse(it!!)
                                disposable.dispose()
                            })
                        }, {
                    onApiListener.onApiError(it.toString())
                })
    }

    fun startRide(baseUI: BaseUI, onApiListener: OnApiListener) {

        disposable = getWebApi().startRide(getBasicAuth()).observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io())
                .subscribe(
                        {
                            val apiResponse = MutableLiveData<ApiResponse>()
                            apiResponse.postValue(it)
                            apiResponse.observe(baseUI, Observer {

                                onApiListener.onApiResponse(it!!)
                                disposable.dispose()
                            })
                        }, {
                    onApiListener.onApiError(it.toString())
                })
    }

    fun endRide(baseUI: BaseUI, onApiListener: OnApiListener) {

        disposable = getWebApi().endRide(getBasicAuth()).observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io())
                .subscribe(
                        {
                            val apiResponse = MutableLiveData<ApiResponse>()
                            apiResponse.postValue(it)
                            apiResponse.observe(baseUI, Observer {

                                onApiListener.onApiResponse(it!!)
                                disposable.dispose()
                            })
                        }, {
                    onApiListener.onApiError(it.toString())
                })
    }

    fun rideComplete(base: Base, onApiListener: OnApiListener) {

        disposable = getWebApi().rideComplete(getBasicAuth()).observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io())
                .subscribe(
                        {
                            val apiResponse = MutableLiveData<ApiResponse>()
                            apiResponse.postValue(it)
                            apiResponse.observe(base, Observer {

                                onApiListener.onApiResponse(it!!)
                                disposable.dispose()
                            })
                        }, {
                    onApiListener.onApiError(it.toString())
                })
    }

    fun realTimeDriverLocation(driverLocationSync: DriverLocationSync, driverInfo: DriverInfo, onApiListener: OnApiListener) {

        disposable = getWebApi().realTimeTrackDriver(driverInfo).observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io())
                .subscribe(
                        {
                            val apiResponse = MutableLiveData<ApiResponse>()
                            apiResponse.postValue(it)
                            apiResponse.observe(driverLocationSync, Observer {

                                onApiListener.onApiResponse(it!!)
                                disposable.dispose()
                            })
                        }, {
                    it.printStackTrace()
                    onApiListener.onApiError(it.toString())
                })
    }

    fun setDriverStatus(baseUI: BaseUI, driverStatus: String, onApiListener: OnApiListener) {
        val auth = getBasicAuth()
        auth.driverStatus = driverStatus
        disposable = getWebApi().setDriverStatus(auth).observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io())
                .subscribe(
                        {
                            val apiResponse = MutableLiveData<ApiResponse>()
                            apiResponse.postValue(it)
                            apiResponse.observe(baseUI, Observer {

                                onApiListener.onApiResponse(it!!)
                                disposable.dispose()
                            })
                        }, {
                    it.printStackTrace()
                    onApiListener.onApiError(it.toString())
                })
    }

//    fun getDriverInfo(baseUI: BaseUI, onApiListener: OnApiListener) {
//
//        disposable = getWebApi().driverInfo(getBasicAuth()).observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io())
//                .subscribe(
//                        {
//                            val apiResponse = MutableLiveData<ApiResponse>()
//                            apiResponse.postValue(it)
//                            apiResponse.observe(baseUI, Observer {
//
//                                onApiListener.onApiResponse(it!!)
//                                disposable.dispose()
//                            })
//                        }, {
//                    onApiListener.onApiError(it.toString())
//                })
//    }

//    fun getRiderInfo(baseUI: BaseUI, onApiListener: OnApiListener) {
//        val rideID = getBasicAuth().rideID
//        if (rideID != "N/A") {
//            disposable = getWebApi().getRiderInfo(rideID).observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io())
//                    .subscribe(
//                            {
//                                val apiResponse = MutableLiveData<ApiResponse>()
//                                apiResponse.postValue(it)
//                                apiResponse.observe(baseUI, Observer {
//
//                                    onApiListener.onApiResponse(it!!)
//                                    disposable.dispose()
//                                })
//                            }, {
//                        it.printStackTrace()
//                        onApiListener.onApiError(it.toString())
//                    })
//        } else Log.e("getRiderInfoAPI", "No RiderInfo's ID found")
//    }

    fun getDriverAndRiderInfo(baseUI: BaseUI, onDriverListener: OnApiListener, onRiderListener: OnApiListener) {
        val auth = getBasicAuth()
        disposable = getWebApi().getDriverInfo(auth).flatMap({ response ->
            val apiResponse = MutableLiveData<ApiResponse>()
            apiResponse.postValue(response)
            apiResponse.observe(baseUI, Observer {

                onDriverListener.onApiResponse(it!!)

            })
            getWebApi().getRiderInfo(auth.rideID)
        }, true).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        {
                            val apiResponse = MutableLiveData<ApiResponse>()
                            apiResponse.postValue(it)
                            apiResponse.observe(baseUI, Observer {

                                onRiderListener.onApiResponse(it!!)


                            })
                        }, {
                    Log.e("getDriverAndRiderAPIs", it.toString())

                    it.printStackTrace()

                }, {
                    Log.v("getDriverAndRiderAPIs", "success")
                    disposable.dispose()
                }
                )

    }
}