package com.webdealer.dashdriver.utils

import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager
import com.google.gson.Gson
import com.webdealer.dashdriver.models.User

object AppStorage {
    private lateinit var prefs: SharedPreferences
    private val gson = Gson()
    private const val keyUser = "user"
    fun init(context: Context) {
        prefs = PreferenceManager.getDefaultSharedPreferences(context)
    }

    fun saveUser(user: User) {
        val s = gson.toJson(user)
        prefs.edit().putString(keyUser, s).apply()
    }

    fun getFCM() = prefs.getString("fcm", "")

    fun setFCM(token: String) = prefs.edit().putString("fcm", token).apply()

    fun getUser() = gson.fromJson(prefs.getString(keyUser, null), User::class.java)

    fun clearSession() = prefs.edit().clear().apply()

    fun isLoggedIn() = prefs.getString(keyUser, "").isNotBlank()

    fun saveTodayEarnings(earnings: String) = prefs.edit().putString("earnings", earnings).apply()

    fun setDriverStatus(status: String) = prefs.edit().putString("status", status).apply()

    fun getDriverStatus() = prefs.getString("status", "1") != "0"

    fun getTodayEarnings(): String = prefs.getString("earnings", "$0.00")
}