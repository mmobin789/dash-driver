package com.webdealer.dashdriver.utils

import android.content.Context
import android.graphics.Bitmap
import android.os.Environment
import android.util.Patterns
import id.zelory.compressor.Compressor
import java.io.File

object Utils {
    fun isValidEmail(email: String): Boolean {
        return email.isNotBlank() && Patterns.EMAIL_ADDRESS.matcher(email).matches()
    }

    // if compression fails return the file intact.
    fun getCompressedFile(context: Context, file: File): File {
        try {
            return Compressor(context)
                    .setMaxWidth(200)
                    .setMaxHeight(200)
                    .setCompressFormat(Bitmap.CompressFormat.PNG)
                    .setDestinationDirectoryPath(Environment.getExternalStoragePublicDirectory(
                            Environment.DIRECTORY_PICTURES).absolutePath)
                    .compressToFile(file)
        } catch (e: Exception) {
            e.printStackTrace()
        }

        return file
    }
}