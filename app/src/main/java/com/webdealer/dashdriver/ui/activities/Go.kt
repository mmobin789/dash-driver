package com.webdealer.dashdriver.ui.activities

import android.os.Bundle
import com.google.android.gms.maps.SupportMapFragment
import com.webdealer.dashdriver.R
import kotlinx.android.synthetic.main.activity_go.*
import kotlinx.android.synthetic.main.go_ui.*

class Go : Base() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_go)
        init()
    }

    private fun init() {
        setToolbarTitle("Booking")
        val map = map as SupportMapFragment
        map.getMapAsync {

        }
        visaUI.setOnClickListener {
            // startActivity(Intent(it.context, Wallet::class.java))
        }
    }
}
