package com.webdealer.dashdriver.ui.fragments

import android.app.DatePickerDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.webdealer.dashdriver.R
import com.webdealer.dashdriver.ui.activities.Verification
import kotlinx.android.synthetic.main.fragment_bc.*
import java.util.*

class BackgroundCheck : BaseUI() {
    private lateinit var verification: Verification

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_bc, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {


        verification = base as Verification

        cbUber.setOnCheckedChangeListener { _, isChecked ->
            etRateUber.visibility = if (isChecked)
                View.VISIBLE
            else View.GONE

            verification.driverBackgroundInfo.uber = isChecked

        }
        cbLyft.setOnCheckedChangeListener { _, isChecked ->
            etRateLyft.visibility = if (isChecked)
                View.VISIBLE
            else View.GONE
            verification.driverBackgroundInfo.lyft = isChecked

        }
        cont.setOnClickListener {
            getViewData()
            if (verification.driverBackgroundInfo.isBackGroundValid())
                verification.onForwardPressed()
            else showToast("Fields Required")

        }

        etExp.setOnClickListener {
            chooseDOB(it, false)
        }

        etDate.setOnClickListener {
            chooseDOB(it, true)
        }

    }

    private fun getViewData() {
        verification.driverBackgroundInfo.firstName = etNameF.text.toString()
        verification.driverBackgroundInfo.middleName = etMName.text.toString()
        verification.driverBackgroundInfo.lastName = etNameL.text.toString()
        verification.driverBackgroundInfo.socialSecurityNo = etSoc.text.toString()
        verification.driverBackgroundInfo.licenseCardNo = etL.text.toString()
        verification.driverBackgroundInfo.city = etCity.text.toString()
        verification.driverBackgroundInfo.state = etState.text.toString()
        verification.driverBackgroundInfo.zipCode = etZip.text.toString()
        verification.driverBackgroundInfo.address = etAddress.text.toString()
        verification.driverBackgroundInfo.uberRating = etRateUber.text.toString().toFloatOrNull()
        verification.driverBackgroundInfo.lyftRating = etRateLyft.text.toString().toFloatOrNull()


    }

    private fun chooseDOB(it: View, isDOB: Boolean) {

        val calendar = Calendar.getInstance()
        val dpDialog = DatePickerDialog(it.context, R.style.TimePickerTheme, DatePickerDialog.OnDateSetListener { view, year, month, dayOfMonth ->
            val date = dayOfMonth.toString() + "/" + (month + 1) + "/" + year.toString()
            if (isDOB) {
                verification.driverBackgroundInfo.dob = date
                etDate.setText(date)
            } else {
                verification.driverBackgroundInfo.expiryDate = date
                etExp.setText(date)
            }

        }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH))
        dpDialog.show()
    }

    companion object {
        fun newInstance() = BackgroundCheck()
    }
}