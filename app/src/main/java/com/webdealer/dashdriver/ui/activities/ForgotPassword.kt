package com.webdealer.dashdriver.ui.activities

import android.content.Intent
import android.os.Bundle
import android.view.View
import com.webdealer.dashdriver.R
import kotlinx.android.synthetic.main.activity_forgot_password.*
import kotlinx.android.synthetic.main.toolbar.*

class ForgotPassword : Base() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_forgot_password)
        signUP.setOnClickListener {
            startActivity(Intent(it.context, SignUP::class.java))
        }
        setToolbarTitle("Password Recovery")
        back.visibility = View.VISIBLE
        back.setOnClickListener {
            onBackPressed()
        }
    }
}
