package com.webdealer.dashdriver.ui.fragments

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.webdealer.dashdriver.R
import kotlinx.android.synthetic.main.fragment_earnings.*

class EarningsFragment : BaseUI() {


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_earnings, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        rv.layoutManager = LinearLayoutManager(context)

        withdrawBtn.setOnClickListener {
            withDrawDetailUI.visibility = View.VISIBLE
        }
    }

    companion object {

        fun newInstance(): EarningsFragment = EarningsFragment()
    }
}