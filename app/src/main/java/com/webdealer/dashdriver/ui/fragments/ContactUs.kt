package com.webdealer.dashdriver.ui.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.webdealer.dashdriver.R

class ContactUs : BaseUI() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_contact_us, container, false)

    }

    companion object {
        private var contactUs: ContactUs? = null
        fun newInstance(): ContactUs {
            if (contactUs == null)
                contactUs = ContactUs()
            return contactUs!!
        }
    }
}