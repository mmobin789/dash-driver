package com.webdealer.dashdriver.ui.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.vansuita.pickimage.listeners.IPickResult
import com.webdealer.dashdriver.R
import com.webdealer.dashdriver.ui.activities.Base
import com.webdealer.dashdriver.ui.activities.Verification
import kotlinx.android.synthetic.main.fragment_profilepic.*
import java.io.File

class ProfilePicture : BaseUI() {


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_profilepic, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        val verification = base as Verification
        takePic.setOnClickListener { v ->
            Base.pickImage(verification, IPickResult {
                verification.profilePicture = File(it.path)
                if (it != null && it.path != null) {
                    Base.loadWithGlide(it.path, photo, false)
                    // verification.setPageNo(2)


                } else showToast("Picture Required")
            })
        }


    }

    companion object {
        fun newInstance() = ProfilePicture()
    }
}