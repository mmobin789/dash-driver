package com.webdealer.dashdriver.ui.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import com.webdealer.dashdriver.R
import com.webdealer.dashdriver.contracts.OnApiListener
import com.webdealer.dashdriver.models.ApiResponse
import com.webdealer.dashdriver.models.Auth
import com.webdealer.dashdriver.ui.activities.Base.Companion.dashDriverImpl
import com.webdealer.dashdriver.ui.activities.Base.Companion.dismissProgressBar
import com.webdealer.dashdriver.ui.activities.Base.Companion.showProgressBar
import com.webdealer.dashdriver.ui.activities.Verification
import com.webdealer.dashdriver.utils.AppStorage
import kotlinx.android.synthetic.main.numpad.*
import kotlinx.android.synthetic.main.verify_pin.*

class PinVerification : BaseUI(), OnApiListener {
    private lateinit var verification: Verification
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.verify_pin, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        numPad()

        verification = base as Verification
        proceed.setOnClickListener {
            verifyPhone()
        }
        setCodeUI()



        rc.setOnClickListener {
            showProgressBar()
            val auth = Auth(verification.email, "")
            dashDriverImpl.resendCode(this, auth, object : OnApiListener {
                override fun onApiResponse(apiResponse: ApiResponse) {
                    dismissProgressBar()

                    showToast(if (apiResponse.status)
                        apiResponse.message
                    else apiResponse.error.description)

                }

                override fun onApiError(error: String) {
                    dismissProgressBar()
                    showToast(error)
                }
            })
        }

    }

    private fun verifyPhone() {

        val auth = Auth(verification.email, "")
        val code = digit1.text.toString() + digit2.text.toString() + digit3.text.toString() + digit4.text.toString()
        if (code.isNotBlank() && code.length == 4) {
            auth.code = code
            showProgressBar()
            dashDriverImpl.verifyPhone(this, auth, this)
        } else
            showToast("Invalid Code")
    }

    override fun onApiResponse(apiResponse: ApiResponse) {
        dismissProgressBar()
        if (apiResponse.status) {
            verification.phoneVerified = apiResponse.status
            val user = AppStorage.getUser()
            user.status = "1"
            AppStorage.saveUser(user)
            showToast("Success")
            verification.onForwardPressed()
        } else showToast(apiResponse.error.description)
    }

    override fun onApiError(error: String) {
        dismissProgressBar()
        showToast(error)
    }

    private fun setCodeUI() {
        var code = "Enter the  4 - digit code sent to you at " + verification.phoneNo.substring(0, 4)
        val x = StringBuilder()
        for (i in 5..verification.phoneNo.length) {
            x.append("X")
        }
        code += x.toString()
        codeTV.text = code
    }


    private fun numPad() {

        val numPadClick = View.OnClickListener { v ->
            when (v.id) {
                11 -> setDisplay("0")
                12 -> backSpace()
                else -> setDisplay(v.id.toString())
            }

        }

        val rows = numpad.childCount
        for (i in 0 until rows) {
            val row = numpad.getChildAt(i) as LinearLayout
            val tvCount = row.childCount
            for (d in 0 until tvCount) {
                val tv = row.getChildAt(d)
                val id = when (i) {
                    0 -> d + 1
                    1 -> d + 4
                    2 -> d + 7
                    else -> d + 10
                }
                tv.id = id
                tv.setOnClickListener(numPadClick)
            }
        }
    }

    private fun setDisplay(number: String) {

        when {
            digit1.length() == 0 -> digit1.setText(number)
            digit2.length() == 0 -> digit2.setText(number)
            digit3.length() == 0 -> digit3.setText(number)
            digit4.length() == 0 -> digit4.setText(number)
        }
    }

    private fun backSpace() {
        when {
            digit4.length() > 0 -> digit4.setText("")
            digit3.length() > 0 -> digit3.setText("")
            digit2.length() > 0 -> digit2.setText("")
            digit1.length() > 0 -> digit1.setText("")
        }
    }


    companion object {
        fun newInstance() = PinVerification()
    }
}