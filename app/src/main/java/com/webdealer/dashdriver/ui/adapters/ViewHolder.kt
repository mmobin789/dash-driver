package com.webdealer.dashdriver.ui.adapters

import android.support.v7.widget.RecyclerView
import android.view.View
import com.webdealer.dashdriver.contracts.OnListItemClickListener
import kotlinx.android.extensions.LayoutContainer

class ViewHolder(private val onListItemClickListener: OnListItemClickListener, override val containerView: View) : RecyclerView.ViewHolder(containerView), LayoutContainer {
    init {
        containerView.setOnClickListener {
            onListItemClickListener.onListItemClicked(adapterPosition)
        }
    }
}