package com.webdealer.dashdriver.ui.activities

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import com.google.android.gms.maps.SupportMapFragment
import com.webdealer.dashdriver.R
import com.webdealer.dashdriver.contracts.OnListItemClickListener
import com.webdealer.dashdriver.models.DateSlot
import com.webdealer.dashdriver.models.TimeSlot
import com.webdealer.dashdriver.ui.adapters.DateSlotAdapter
import com.webdealer.dashdriver.ui.adapters.TimeSlotAdapter
import kotlinx.android.synthetic.main.activity_ride_later.*
import java.text.SimpleDateFormat
import java.util.*

class RideLater : Base() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_ride_later)
        setToolbarTitle("Ride Later")
        init()
    }

    private fun getTimeSlots(): List<TimeSlot> {
        val timeSlots = resources.getStringArray(R.array.times)
        val timeSlotList = mutableListOf<TimeSlot>()
        for (i in 0 until timeSlots.size) {
            val isSet = i == 1
            val timeSlot = TimeSlot(timeSlots[i], isSet)
            timeSlotList.add(timeSlot)

        }
        return timeSlotList
    }

    private fun init() {
        val map = map as SupportMapFragment
        map.getMapAsync {
            it.uiSettings.isZoomControlsEnabled = true
            it.uiSettings.isZoomGesturesEnabled = true
        }
        var layoutManager = getLinearLayoutManager(this)
        layoutManager.orientation = LinearLayoutManager.HORIZONTAL
        rvTime.layoutManager = layoutManager
        layoutManager = getLinearLayoutManager(this)
        layoutManager.orientation = LinearLayoutManager.HORIZONTAL
        rvDate.layoutManager = layoutManager
        initDatesAdapter()
        initTimeAdapter()

    }

    private fun initDatesAdapter() {

        val currentTimeC: Calendar = Calendar.getInstance()
        val dates = mutableListOf<DateSlot>()
        val monthFormat = SimpleDateFormat("MMM", Locale.getDefault())
        for (i in 0..7) {
            val isSet = i == 0
            val dateSlot = DateSlot(monthFormat.format(currentTimeC.time), currentTimeC.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.SHORT, Locale.getDefault()), currentTimeC.get(Calendar.DAY_OF_MONTH), isSet)
            dates.add(dateSlot)
            currentTimeC.add(Calendar.DATE, 1)


        }
        var lastCheckedPosition = 0
        rvDate.adapter = DateSlotAdapter(dates, object : OnListItemClickListener {
            override fun onListItemClicked(position: Int) {
                dates[lastCheckedPosition].isSet = false
                rvDate.adapter.notifyItemChanged(lastCheckedPosition)
                dates[position].isSet = true
                rvDate.adapter.notifyItemChanged(position)
                lastCheckedPosition = position
            }
        })


    }


    private fun initTimeAdapter() {
        var lastCheckedPosition = 1
        val timeSlots = getTimeSlots()
        rvTime.adapter = TimeSlotAdapter(timeSlots, object : OnListItemClickListener {
            override fun onListItemClicked(position: Int) {
                timeSlots[lastCheckedPosition].isSet = false
                rvTime.adapter.notifyItemChanged(lastCheckedPosition)
                timeSlots[position].isSet = true
                rvTime.adapter.notifyItemChanged(position)
                lastCheckedPosition = position
            }
        })
    }
}
