package com.webdealer.dashdriver.ui.adapters

import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import com.webdealer.dashdriver.ui.fragments.*

class SignUpPager(fm: FragmentManager) : FragmentPagerAdapter(fm) {
    override fun getItem(position: Int): BaseUI {
        return when (position) {
            0 -> {
                PinVerification.newInstance()
            }
            1 -> {
                BackgroundCheck.newInstance()
            }
            2 -> {
                VehicleInformation.newInstance()
            }
            3 -> {
                ProfilePicture.newInstance()
            }
            4 -> {
                DriverLicense.newInstance()
            }
            5 -> {
                VehicleRegistrationBook.newInstance()
            }
            6 -> {
                InsuranceDocument.newInstance()
            }
            7 -> {
                CarPicture.newInstance()
            }
            else -> {
                DashPartner.newInstance()
            }
        }
    }

    override fun getCount(): Int {
        return 9
    }
}