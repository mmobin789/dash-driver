package com.webdealer.dashdriver.ui.activities

import android.content.Intent
import android.os.Bundle
import android.util.Log
import com.facebook.CallbackManager
import com.facebook.FacebookCallback
import com.facebook.FacebookException
import com.facebook.GraphRequest
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.twitter.sdk.android.core.*
import com.twitter.sdk.android.core.identity.TwitterAuthClient
import com.twitter.sdk.android.core.models.User
import com.webdealer.dashdriver.R
import com.webdealer.dashdriver.api.Instagram
import com.webdealer.dashdriver.contracts.OnApiListener
import com.webdealer.dashdriver.contracts.OnInstagramAccessListener
import com.webdealer.dashdriver.contracts.OnInstagramLoginListener
import com.webdealer.dashdriver.models.ApiResponse
import com.webdealer.dashdriver.models.Auth
import com.webdealer.dashdriver.utils.AppStorage
import com.webdealer.dashdriver.utils.Utils
import io.reactivex.Observable
import kotlinx.android.synthetic.main.activity_sign_up.*

class SignUP : Base(), OnInstagramAccessListener {

    var name = ""
    var email = ""
    var password = ""

    private var callbackManager: CallbackManager? = null
    private var twitterAuthClient: TwitterAuthClient? = null
    private var loginType = Login.facebook


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up)
        init()
    }

    private fun init() {
        signUP.setOnClickListener {
            validate()
        }
        login.setOnClickListener {
            onBackPressed()
        }
        fbLogin.setOnClickListener {
            fbLogin()
        }
        instagramLogin.setOnClickListener {
            loginInstagram()
        }
        fourSQ.setOnClickListener {
            // fourSquare()
        }
    }

    private fun validate() {
        email = etEmail.text.toString()
        password = etPassword.text.toString()
        val confirmPassword = etCPass.text.toString()
        name = etNameF.text.toString() + " " + etNameL.text.toString()
        val phone = etPhone.text.toString()
        val fullNumber = ccp.selectedCountryCodeWithPlus + phone
        val valid = email.isNotBlank() && password.isNotBlank() && name.isNotBlank() && phone.isNotBlank()
        if (valid) {
            if (password.length == 8 && password == confirmPassword) {
                signUp(fullNumber)
            } else if (password.length < 8) showToast("Password Must be 8 characters.")
            else if (!Utils.isValidEmail(email)) showToast("Invalid Email.")
            else showToast("Password mismatched.")
        } else showToast("Fields Required")
    }

    private fun signUp(fullPhoneNumber: String) {
        Observable.just(Auth(name, email, fullPhoneNumber, password)).subscribe {
            if (it.isValid()) {
                showProgressBar()
                dashDriverImpl.register(this, it, object : OnApiListener {
                    override fun onApiResponse(apiResponse: ApiResponse) {
                        dismissProgressBar()
                        if (apiResponse.status) {
                            AppStorage.saveUser(apiResponse.data.user)
                            // setPageNo(3)
                            val verification = Intent(this@SignUP, Verification::class.java)
                            verification.putExtra("email", apiResponse.data.user.email)
                            verification.putExtra("name", apiResponse.data.user.name)
                            verification.putExtra("phone", apiResponse.data.user.phone)
                            startActivity(verification)
                        } else showToast(apiResponse.error.description)
                    }

                    override fun onApiError(error: String) {
                        dismissProgressBar()
                        showToast(error)
                    }
                })
            } else showToast("Fields Required")

        }
    }

    private fun getFacebookProfile(loginResult: LoginResult) {

        val graphRequest = GraphRequest.newMeRequest(loginResult.accessToken) { `object`, _ ->
            Log.i("FacebookGraphAPI", `object`.toString())
//            val picture = Profile.getCurrentProfile().getProfilePictureUri(200, 200)
            etNameF.setText(`object`.getString("first_name"))
            etNameL.setText(`object`.getString("last_name"))
            etEmail.setText(`object`.getString("email"))
        }
        val params = Bundle()
        params.putString("fields", "name,first_name,last_name,email,picture")
        graphRequest.parameters = params
        graphRequest.executeAsync()
    }

    private fun fbLogin() {
        callbackManager = CallbackManager.Factory.create()
        val loginManager = LoginManager.getInstance()
        loginManager.logInWithReadPermissions(this, listOf("public_profile", "email"))
        loginManager.registerCallback(callbackManager, object : FacebookCallback<LoginResult> {

            override fun onSuccess(result: LoginResult) {
                getFacebookProfile(result)
            }

            override fun onCancel() {

            }

            override fun onError(error: FacebookException) {
                error.printStackTrace()
            }
        })
    }

    private fun twitterLogin() {
        loginType = Login.twitter
        Twitter.initialize(this)
        twitterAuthClient = TwitterAuthClient()
        twitterAuthClient!!.authorize(this, object : Callback<TwitterSession>() {
            override fun success(result: Result<TwitterSession>) {

                //  getTwitterEmail(result)

                val user = TwitterCore.getInstance().getApiClient(result.data).accountService.verifyCredentials(false, false, true)
                user.enqueue(object : Callback<User>() {
                    override fun success(resultUser: Result<User>) {
                        etEmail.setText(resultUser.data.email)
                        etNameF.setText(resultUser.data.name)
//                        val signUp = Intent(this@Login, Registration::class.java)
//                        signUp.putExtra("username", result.data.userName)
//                        signUp.putExtra("name", resultUser.data.name)
//                        signUp.putExtra("picture", resultUser.data.profileImageUrl)
//                        signUp.putExtra("email", resultUser.data.email)
//                        signUp.putExtra("twitterUser", true)
//                        startActivity(signUp)
                    }

                    override fun failure(exception: TwitterException) {
                        Log.e("Twitter", exception.toString())
                    }
                })

            }

            override fun failure(exception: TwitterException) {
                Log.e("Twitter", exception.toString())
            }
        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (loginType == Login.facebook)
            callbackManager!!.onActivityResult(requestCode, resultCode, data)
        else if (loginType == Login.twitter) twitterAuthClient!!.onActivityResult(requestCode, resultCode, data)
//        else {
//            if (requestCode == 32) {
//                val authCodeResponse = FoursquareOAuth.getAuthCodeFromResult(resultCode, data)
//                FoursquareOAuth.getTokenExchangeIntent(this, getString(R.string.fourSQClient), getString(R.string.fourSQSecret), authCodeResponse.code);
//                startActivityForResult(intent, 22);
//            } else {
//                val accessTokenResponse = FoursquareOAuth.getTokenFromResult(resultCode, data)
//                showToast(accessTokenResponse.accessToken)
//            }
//        }
    }

    private fun loginInstagram() {
        Instagram.loginInstagram(this, this)
    }

    override fun onInstagramAccessGranted(accessToken: String) {
        dashDriverImpl.getInstagramUser(this, accessToken, object : OnInstagramLoginListener {
            override fun onSuccess(instagram: Instagram) {
                val user = instagram.data
                etNameF.setText(user.username)
                etNameL.setText(user.fullName)
            }


        })
    }

    //    private fun fourSquare() {
//        loginType = Login.FourSquare
//        val foursquareOAuth = FoursquareOAuth.getConnectIntent(this, getString(R.string.fourSQClient))
//        startActivityForResult(foursquareOAuth, 32)
//    }
    private enum class Login {
        twitter, facebook, FourSquare
    }
}
