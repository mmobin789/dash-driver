package com.webdealer.dashdriver.ui.fragments

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.webdealer.dashdriver.R
import com.webdealer.dashdriver.contracts.OnApiListener
import com.webdealer.dashdriver.models.ApiResponse
import com.webdealer.dashdriver.ui.activities.Base.Companion.dashDriverImpl
import com.webdealer.dashdriver.ui.activities.Base.Companion.dismissProgressBar
import com.webdealer.dashdriver.ui.activities.Base.Companion.showProgressBar
import com.webdealer.dashdriver.ui.activities.MainActivity
import com.webdealer.dashdriver.ui.activities.Verification
import com.webdealer.dashdriver.utils.AppStorage
import kotlinx.android.synthetic.main.fragment_dash_partner.*

class DashPartner : BaseUI(), OnApiListener {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_dash_partner, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val verification = base as Verification
        dashGo.setOnClickListener {
            dashGo.setImageResource(R.drawable.dashgo_selected)
            dashX.setImageResource(R.drawable.dashx)
            verification.carType = "DashGo"

        }
        dashX.setOnClickListener {
            dashGo.setImageResource(R.drawable.dashgo)
            dashX.setImageResource(R.drawable.dashx_selected)
            verification.carType = "DashX"
        }
        cont.setOnClickListener {

            if (!verification.phoneVerified)
                showToast("Phone Number verification Required.")
            else if (verification.vehicleBook == null || verification.license == null || verification.insuranceDoc == null || verification.carPicture == null || verification.profilePicture == null) {
                showToast("Driver Documents Required.")
            } else {
                showProgressBar()
                dashDriverImpl.driverDocs(this, verification.carType, verification.license!!, verification.vehicleBook!!, verification.profilePicture!!, verification.carPicture!!, verification.insuranceDoc!!, verification.driverBackgroundInfo, this)
            }
        }
    }

    override fun onApiResponse(apiResponse: ApiResponse) {
        dismissProgressBar()
        showToast(if (apiResponse.status) {
            AppStorage.saveUser(apiResponse.data.user)
            startActivity(Intent(context, MainActivity::class.java))
            apiResponse.message
        } else apiResponse.error.description)

    }

    override fun onApiError(error: String) {
        dismissProgressBar()
        showToast(error)
    }

    companion object {
        fun newInstance() = DashPartner()
    }
}