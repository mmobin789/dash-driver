package com.webdealer.dashdriver.ui.adapters

import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.webdealer.dashdriver.R
import com.webdealer.dashdriver.contracts.OnListItemClickListener
import com.webdealer.dashdriver.ui.activities.RideHistoryDetails

class RideHistoryAdapter(private val onListItemClickListener: OnListItemClickListener) : RecyclerView.Adapter<ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val vh = ViewHolder(onListItemClickListener, LayoutInflater.from(parent.context).inflate(R.layout.ride_row, parent, false))
        vh.containerView.setOnClickListener {

            parent.context.startActivity(Intent(parent.context, RideHistoryDetails::class.java))
        }

        return vh
    }

    override fun getItemCount(): Int {
        return 10
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

    }

}