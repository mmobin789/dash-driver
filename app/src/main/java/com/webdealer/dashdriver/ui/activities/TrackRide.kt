package com.webdealer.dashdriver.ui.activities

import android.os.Bundle
import android.view.View
import com.webdealer.dashdriver.R
import kotlinx.android.synthetic.main.toolbar.*

class TrackRide : Base() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_track_ride)
        setToolbarTitle("Book a Ride")
        cancel.visibility = View.VISIBLE
    }
}
