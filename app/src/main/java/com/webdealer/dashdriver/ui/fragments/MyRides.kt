package com.webdealer.dashdriver.ui.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.webdealer.dashdriver.R
import com.webdealer.dashdriver.ui.adapters.MyRidesPagerAdapter
import kotlinx.android.synthetic.main.myrides.*

class MyRides : BaseUI() {


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.myrides, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        pager.adapter = MyRidesPagerAdapter(childFragmentManager)
        tabL.setupWithViewPager(pager)

    }

    companion object {
        private var myRides: MyRides? = null
        fun newInstance(): MyRides {
            if (myRides == null)
                myRides = MyRides()
            return myRides!!
        }
    }
}