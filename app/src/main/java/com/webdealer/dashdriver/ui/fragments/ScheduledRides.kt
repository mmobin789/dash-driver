package com.webdealer.dashdriver.ui.fragments

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.webdealer.dashdriver.R
import com.webdealer.dashdriver.ui.activities.TrackRide
import kotlinx.android.synthetic.main.fragment_scheduled.*


class ScheduledRides : BaseUI() {
    companion object {
        private var scheduledRides: ScheduledRides? = null
        fun newInstance(): ScheduledRides {
            if (scheduledRides == null)
                scheduledRides = ScheduledRides()
            return scheduledRides!!
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_scheduled, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        trackDriver.setOnClickListener {
            startActivity(Intent(it.context, TrackRide::class.java))
        }
    }
}