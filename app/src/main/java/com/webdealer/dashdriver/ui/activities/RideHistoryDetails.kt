package com.webdealer.dashdriver.ui.activities

import android.os.Bundle
import android.view.View
import com.google.android.gms.maps.SupportMapFragment
import com.webdealer.dashdriver.R
import kotlinx.android.synthetic.main.activity_ride_history_details.*

class RideHistoryDetails : Base() {
    private var isClicked = true
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_ride_history_details)
        init()
    }

    private fun init() {
        val map = map as SupportMapFragment
        map.getMapAsync {

        }
        line.visibility = View.GONE
        paymentIV.setOnClickListener {
            val detailUIVisibility = if (isClicked) {
                isClicked = false
                View.GONE
            } else {
                isClicked = true
                View.VISIBLE


            }
            paymentDetail.visibility = detailUIVisibility
        }
        setToolbarTitle("My Rides")
    }
}
