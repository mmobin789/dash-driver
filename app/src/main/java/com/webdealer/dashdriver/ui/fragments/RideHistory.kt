package com.webdealer.dashdriver.ui.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.webdealer.dashdriver.R
import com.webdealer.dashdriver.contracts.OnListItemClickListener
import com.webdealer.dashdriver.ui.activities.Base
import com.webdealer.dashdriver.ui.adapters.RideHistoryAdapter
import kotlinx.android.synthetic.main.fragment_history.*

class RideHistory : BaseUI() {
    companion object {
        private var myRides: RideHistory? = null
        fun newInstance(): RideHistory {
            if (myRides == null)
                myRides = RideHistory()
            return myRides!!
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_history, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        rv.layoutManager = Base.getLinearLayoutManager(context!!)
        rv.adapter = RideHistoryAdapter(object : OnListItemClickListener {
            override fun onListItemClicked(position: Int) {

            }
        })
    }
}