package com.webdealer.dashdriver.ui.activities

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.Gravity
import android.view.View
import com.webdealer.dashdriver.R
import com.webdealer.dashdriver.contracts.OnApiListener
import com.webdealer.dashdriver.models.ApiResponse
import com.webdealer.dashdriver.models.Auth
import com.webdealer.dashdriver.models.HomeNavigation
import com.webdealer.dashdriver.ui.fragments.*
import com.webdealer.dashdriver.utils.AppStorage
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.drawer.*
import kotlinx.android.synthetic.main.fragment_earnings.*
import kotlinx.android.synthetic.main.toolbar.*

class MainActivity : Base(), OnApiListener {

    lateinit var baseUI: BaseUI
    var driverLocationSync: Intent? = null
    var homeNavigation = HomeNavigation.Home
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }

    private fun init() {
        val user = AppStorage.getUser()



        if (user.status.isNullOrBlank() || user.status == "0") {
            startActivity(Intent(this, Verification::class.java))
            finish()
        }


        val navClickEvent = View.OnClickListener {
            if (statusUI.isShown)
                statusUI.visibility = View.GONE
            baseUI = when (it.id) {
                help.id -> {
                    ContactUs.newInstance()
                }
                rides.id -> {
                    MyRides.newInstance()
                }
                earnings.id -> {
                    EarningsFragment.newInstance()
                }
                settings.id -> {
                    Settings.newInstance()
                }
                else -> {

                    Home.newInstance()
                }
            }
            drawerL.closeDrawer(Gravity.START)
            setFragment()
        }
        setToolbarTitle("Dash")
        baseUI = Home.newInstance()
        setFragment()
        home.setOnClickListener(navClickEvent)
        help.setOnClickListener(navClickEvent)
        rides.setOnClickListener(navClickEvent)
        earnings.setOnClickListener(navClickEvent)
        settings.setOnClickListener(navClickEvent)
        drawerIV.visibility = View.VISIBLE
        drawerIV.setOnClickListener {
            drawerL.openDrawer(Gravity.START)
        }
        dashDriverImpl.fcm(this, Auth(user.email, ""), this)

        if (intent.hasExtra("rideID")) {
            fcmData = Bundle()
            fcmData!!.putString("rideID", intent.getStringExtra("rideID"))
        }
    }

    override fun onApiResponse(apiResponse: ApiResponse) {

        Log.i("FCMApi", if (apiResponse.status)
            apiResponse.message
        else apiResponse.error.description)

    }

    override fun onApiError(error: String) {
        showToast(error)
    }

    private fun setFragment() {
        supportFragmentManager.beginTransaction().replace(R.id.container, baseUI).commit()
    }

    private var onBackForExit = -1

    private fun home() {
        if (baseUI is Home) {
            val home = baseUI as Home
            if (homeNavigation == HomeNavigation.Home) {

                super.onBackPressed()
            } else {
                when (homeNavigation) {

                    HomeNavigation.ConfirmArrival -> {
                        home.rideUI()

                    }
                    HomeNavigation.StartRide -> {
                        home.confirmArrivalUI()
                    }
                    HomeNavigation.EndRide -> {
                        home.startRideUI()

                    }
                    else -> {
                        home.homeUI()
                        onBackForExit++
                        if (onBackForExit == 1)
                            super.onBackPressed()
                    }

                }


            }
        }

    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        if (baseUI is Home)
            baseUI.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    private fun earnings() {
        if (baseUI is EarningsFragment) {
            val earning = baseUI as EarningsFragment
            if (earning.withDrawDetailUI.isShown) {
                earning.withDrawDetailUI.visibility = View.GONE

            } else
                super.onBackPressed()
        }


    }

    override fun onBackPressed() {
        home()
        earnings()
    }


}
