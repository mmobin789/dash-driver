package com.webdealer.dashdriver.ui.activities

import android.os.Bundle
import android.view.View
import com.webdealer.dashdriver.R
import kotlinx.android.synthetic.main.toolbar.*

class ChangePassword : Base() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_change_password)
        setToolbarTitle("Change Password")
        back.visibility = View.VISIBLE
        back.setOnClickListener {
            onBackPressed()
        }
    }
}
