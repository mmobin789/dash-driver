package com.webdealer.dashdriver.ui.activities

import android.os.Bundle
import com.webdealer.dashdriver.R
import kotlinx.android.synthetic.main.activity_rate_rider.*

class RateRider : Base() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_rate_rider)
        setToolbarTitle("Rate the rider")
        map.onCreate(savedInstanceState)
        map.getMapAsync {

            it.uiSettings.setAllGesturesEnabled(true)
            it.uiSettings.isZoomControlsEnabled = true
        }
    }

    override fun onResume() {
        super.onResume()
        map.onResume()
    }
}
