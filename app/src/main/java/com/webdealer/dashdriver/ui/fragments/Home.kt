package com.webdealer.dashdriver.ui.fragments

import android.annotation.SuppressLint
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.gms.location.*
import com.google.android.gms.maps.GoogleMap
import com.webdealer.dashdriver.R
import com.webdealer.dashdriver.contracts.OnApiListener
import com.webdealer.dashdriver.models.*
import com.webdealer.dashdriver.services.DriverLocationSync
import com.webdealer.dashdriver.ui.activities.Base.Companion.dashDriverImpl
import com.webdealer.dashdriver.ui.activities.Base.Companion.dismissProgressBar
import com.webdealer.dashdriver.ui.activities.Base.Companion.hasLocationPermission
import com.webdealer.dashdriver.ui.activities.Base.Companion.showProgressBar
import com.webdealer.dashdriver.ui.activities.Base.Companion.zoomToMyLocation
import com.webdealer.dashdriver.ui.activities.EndRidePayment
import com.webdealer.dashdriver.ui.activities.MainActivity
import com.webdealer.dashdriver.utils.AppStorage
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.android.synthetic.main.toolbar.*


class Home : BaseUI() {
    lateinit var main: MainActivity

    private lateinit var googleMap: GoogleMap
    private var fusedLocationProviderClient: FusedLocationProviderClient? = null
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_home, container, false)
    }


    fun confirmArrivalUI() {
        main.setToolbarTitle("In Transit")
        main.homeNavigation = HomeNavigation.ConfirmArrival
        confirmArrivalUI.visibility = View.VISIBLE
        transitUI.visibility = View.VISIBLE
        rideUI.visibility = View.GONE
        cancelonArrivalUI.visibility = View.VISIBLE
        confirmArrival.text = "Confirm Arrival"
        confirmArrival.setOnClickListener {
            showProgressBar()
            dashDriverImpl.confirmArrival(this, object : OnApiListener {
                override fun onApiResponse(apiResponse: ApiResponse) {
                    dismissProgressBar()
                    showToast(if (apiResponse.status) {
                        startRideUI()
                        "Arrival Confirmed"
                    } else apiResponse.error.description)
                }

                override fun onApiError(error: String) {
                    dismissProgressBar()
                    showToast(error)
                }
            })
        }
        cancelonArrivalUI.setOnClickListener {
            cancelRide.performClick()
        }
    }

    fun startRideUI() {
        main.setToolbarTitle("In Transit")
        main.homeNavigation = HomeNavigation.StartRide
        confirmArrivalUI.visibility = View.VISIBLE
        transitUI.visibility = View.VISIBLE
        rideUI.visibility = View.GONE
        confirmArrival.text = "Start Ride"
        cancelonArrivalUI.visibility = View.GONE
        confirmArrival.setOnClickListener {
            showProgressBar()
            dashDriverImpl.startRide(this, object : OnApiListener {
                override fun onApiResponse(apiResponse: ApiResponse) {
                    dismissProgressBar()
                    showToast(if (apiResponse.status) {
                        endRideUI()
                        "Ride Started"
                    } else apiResponse.error.description)

                }

                override fun onApiError(error: String) {
                    dismissProgressBar()
                    showToast(error)
                }
            })
        }

    }

    private fun endRideUI() {
        main.setToolbarTitle("In Transit")
        main.homeNavigation = HomeNavigation.EndRide
        confirmArrivalUI.visibility = View.VISIBLE
        transitUI.visibility = View.VISIBLE
        rideUI.visibility = View.GONE
        confirmArrival.text = "End Ride"
        cancelonArrivalUI.visibility = View.GONE
        confirmArrival.setOnClickListener {
            showProgressBar()
            dashDriverImpl.endRide(this, object : OnApiListener {
                override fun onApiResponse(apiResponse: ApiResponse) {
                    dismissProgressBar()
                    showToast(if (apiResponse.status) {
                        // add bill object the api returns into intent
                        endRideReceivePayment(apiResponse.bill)

                        "Ride Ended"

                    } else apiResponse.error.description)

                }

                override fun onApiError(error: String) {
                    dismissProgressBar()
                    showToast(error)
                }
            })
        }

    }

    private fun endRideReceivePayment(bill: Bill) {
        val end = Intent(context, EndRidePayment::class.java)
        end.putExtra("bill", bill)
        startActivity(end)
    }

    private fun deliveryUI() {
        main.setToolbarTitle("Delivery")
        main.homeNavigation = HomeNavigation.RideOrDelivery
        deliveryUI.visibility = View.VISIBLE
        homeUI.visibility = View.GONE
        confirmDelivery.setOnClickListener {

        }

    }

    fun rideUI() {
        main.statusUI.visibility = View.GONE
        main.setToolbarTitle("Searching for Ride")
        rideUI.visibility = View.VISIBLE
        homeUI.visibility = View.GONE
        confirmArrivalUI.visibility = View.GONE
        transitUI.visibility = View.GONE
        main.homeNavigation = HomeNavigation.RideOrDelivery
        confirmRide.setOnClickListener {
            showProgressBar()
            dashDriverImpl.acceptRide(this, object : OnApiListener {
                override fun onApiResponse(apiResponse: ApiResponse) {
                    dismissProgressBar()
                    showToast(if (apiResponse.status) {
                        confirmArrivalUI()
                        "Ride Started"
                    } else apiResponse.error.description)
                }

                override fun onApiError(error: String) {
                    dismissProgressBar()
                    showToast(error)
                }

            })
            // confirmArrivalUI()
        }
        cancelRide.setOnClickListener {
            homeUI()
            //   showProgressBar()
//            dashDriverImpl.cancelRide(this, object : OnApiListener {
//                override fun onApiResponse(apiResponse: ApiResponse) {
//                    dismissProgressBar()
//                    showToast(if (apiResponse.status) {
//                        "Ride Cancelled"
//                    } else apiResponse.error.description)
//                }
//
//                override fun onApiError(error: String) {
//                    dismissProgressBar()
//                    showToast(error)
//                }

//
//            })
//            // confirmArrivalUI()
//        }

        }
    }

    fun homeUI() {
        main.setToolbarTitle("Dash")
        main.statusUI.visibility = View.VISIBLE
        homeUI.visibility = View.VISIBLE
        deliveryUI.visibility = View.GONE
        rideUI.visibility = View.GONE
        confirmArrivalUI.visibility = View.GONE
        transitUI.visibility = View.GONE
    }

    @SuppressLint("MissingPermission")
    private fun getUserLocation() {
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(base)
        val locationRequest = LocationRequest.create()
        locationRequest.interval = 20000
        locationRequest.fastestInterval = 10000
        locationRequest.priority = LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY
        fusedLocationProviderClient!!.requestLocationUpdates(locationRequest, locationCallback, null)
    }


    private object locationCallback : LocationCallback() {

        override fun onLocationResult(p0: LocationResult?) {
            if (p0 != null) {
                val lat = p0.lastLocation.latitude
                val lng = p0.lastLocation.longitude
                zoomToMyLocation(home!!.googleMap, lat, lng)

            }
        }
    }

    override fun onPause() {
        super.onPause()
        map.onPause()
    }

    override fun onResume() {
        super.onResume()
        map.onResume()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        setupMap()
        main = base as MainActivity
        main.statusUI.visibility = View.VISIBLE

        val driverStatus = AppStorage.getDriverStatus()
        main.switchHome.isChecked = driverStatus

        main.statusTV.text = if (driverStatus) "Online" else "Offline"

        main.switchHome.setOnCheckedChangeListener { _, isChecked ->
            var status = "1"
            main.homeNavigation = if (isChecked) {

                main.statusTV.text = "Online"
                //     rideUI()
                HomeNavigation.RideOrDelivery
            } else {
                status = "0"
                main.statusTV.text = "Offline"
                homeUI()
                HomeNavigation.Home
            }

            showProgressBar()
            dashDriverImpl.setDriverStatus(this, status, object : OnApiListener {
                override fun onApiResponse(apiResponse: ApiResponse) {
                    dismissProgressBar()
                    if (apiResponse.status) {
                        AppStorage.setDriverStatus(apiResponse.driverStatus)
                    }

                }

                override fun onApiError(error: String) {
                    dismissProgressBar()
                    showToast(error)
                }
            })
        }
        earningTotal.text = AppStorage.getTodayEarnings()


        //     delay(1, Runnable { getDriverInfo() })


        updateUI()

        driverLocationService()
        //     hrs.text = driverInfo.todayOnlineHours

    }


    private fun driverLocationService() {
        main.driverLocationSync = Intent(context, DriverLocationSync::class.java)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            main.startForegroundService(main.driverLocationSync)
        } else {
            main.startService(main.driverLocationSync)
        }
    }

    private fun setupMap() {
        map.onCreate(null)
        map.getMapAsync {
            map.onResume()
            it.uiSettings.setAllGesturesEnabled(true)
            it.uiSettings.isZoomControlsEnabled = true
            googleMap = it
            showCurrentLocation()

        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        if (requestCode == 3 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED)
            showCurrentLocation()
        else showToast("Location Permission Required to show your current location.")
    }

    @SuppressLint("MissingPermission")
    private fun showCurrentLocation() {
        if (hasLocationPermission(main)) {
            googleMap.isMyLocationEnabled = true
            getUserLocation()
        }
    }

    private fun updateDriverInfo(driverInfo: DriverInfo) {
        main.runOnUiThread {
            miles.text = driverInfo.lastTripMiles
            earning.text = driverInfo.lastTripEarnings
        }

    }

    private fun updateUI() {

        dashDriverImpl.getDriverAndRiderInfo(this, handleDriverResponse(), handleRiderResponse())
    }

    private fun updateRiderInfo(riderInfo: RiderInfo) {
        main.runOnUiThread {
            name.text = riderInfo.name
        }


    }

    private fun handleDriverResponse() = object : OnApiListener {
        override fun onApiResponse(apiResponse: ApiResponse) {
            if (apiResponse.status) {
                val driverInfo = apiResponse.driverInfo
                updateDriverInfo(driverInfo)
            }

        }

        override fun onApiError(error: String) {
            showToast(error)
        }
    }

    private fun handleRiderResponse() = object : OnApiListener {
        override fun onApiResponse(apiResponse: ApiResponse) {
            if (apiResponse.status) {
                rideUI()
                updateRiderInfo(apiResponse.rideInfo)


            }
        }

        override fun onApiError(error: String) {
            showToast(error)

        }
    }


    //    private fun getDriverInfo() {
//        showProgressBar()
//        dashDriverImpl.getDriverInfo(this, object : OnApiListener {
//            override fun onApiResponse(apiResponse: ApiResponse) {
//                dismissProgressBar()
//                val driverInfo = apiResponse.driverInfo
//                miles.text = driverInfo.lastTripMiles
//                earning.text = driverInfo.lastTripEarnings
//
//
//            }
//
//            override fun onApiError(error: String) {
//                dismissProgressBar()
//                showToast(error)
//            }
//        })
    override fun onDestroy() {
        destroyUI()
        super.onDestroy()

    }

    private fun destroyUI() {
        if (map != null)
            map.onDestroy()
        if (fusedLocationProviderClient != null)
            fusedLocationProviderClient!!.removeLocationUpdates(locationCallback)
    }


    companion object {
        private var home: Home? = null
        fun newInstance(): Home {
            if (home == null)
                home = Home()
            return home!!
        }
    }

}