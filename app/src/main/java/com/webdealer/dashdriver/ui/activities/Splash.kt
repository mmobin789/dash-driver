package com.webdealer.dashdriver.ui.activities

import android.content.Intent
import android.os.Bundle
import android.view.Window
import android.view.WindowManager
import com.webdealer.dashdriver.R
import com.webdealer.dashdriver.utils.AppStorage

class Splash : Base() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)
        setContentView(R.layout.activity_splash)
        delay(3, Runnable {
            onBackPressed()
            if (AppStorage.isLoggedIn()) {
                startActivity(Intent(this, MainActivity::class.java))
            } else
                startActivity(Intent(this@Splash, Login::class.java))
        })

        fcmData = intent.extras

    }
}
