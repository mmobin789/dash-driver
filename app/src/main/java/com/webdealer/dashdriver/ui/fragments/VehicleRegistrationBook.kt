package com.webdealer.dashdriver.ui.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.vansuita.pickimage.listeners.IPickResult
import com.webdealer.dashdriver.R
import com.webdealer.dashdriver.ui.activities.Base.Companion.loadWithGlide
import com.webdealer.dashdriver.ui.activities.Base.Companion.pickImage
import com.webdealer.dashdriver.ui.activities.Verification
import kotlinx.android.synthetic.main.fragment_driver_license.*
import java.io.File

class VehicleRegistrationBook : BaseUI() {


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_driver_license, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        photo.setImageResource(R.drawable.registeration)
        label.text = "Take a Photo of your Vehicle Registration Book"

        val verification = base as Verification
        takePic.setOnClickListener {
            pickImage(verification, IPickResult {
                verification.vehicleBook = File(it.path)
                if (it != null && it.path != null) {
                    loadWithGlide(it.path, photo, false)
                    // verification.setPageNo(3)

                } else showToast("Vehicle Registration Required")
            })
        }

    }

    companion object {
        fun newInstance() = VehicleRegistrationBook()
    }
}