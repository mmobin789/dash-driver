package com.webdealer.dashdriver.ui.activities

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import com.webdealer.dashdriver.R
import com.webdealer.dashdriver.contracts.OnApiListener
import com.webdealer.dashdriver.models.ApiResponse
import com.webdealer.dashdriver.utils.AppStorage
import com.webdealer.dashdriver.utils.Utils
import io.reactivex.Observable
import kotlinx.android.synthetic.main.activity_login.*

class Login : Base() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        signUP.setOnClickListener {
            startActivity(Intent(it.context, SignUP::class.java))
        }
        login.setOnClickListener {
            val email = etEmail.text.toString()
            val password = etPassword.text.toString()
            if (!Utils.isValidEmail(email))
                showToast("Invalid Email")
            else if (password.length < 8)
                showToast("Password Must be 8 characters.")
            else
                login(email, password)
        }
        forgotPassword.setOnClickListener {
            startActivity(Intent(it.context, ForgotPassword::class.java))
        }

        hasLocationPermission(this)
    }

    private fun login(email: String, password: String) {
        Observable.just(Utils.isValidEmail(email) && password.isNotBlank()).subscribe {
            if (it) {
                showProgressBar()
                dashDriverImpl.login(this, email, password, object : OnApiListener {
                    override fun onApiResponse(apiResponse: ApiResponse) {
                        dismissProgressBar()
                        if (apiResponse.status) {
                            AppStorage.saveUser(apiResponse.data.user)
                            startActivity(Intent(this@Login, MainActivity::class.java))
                            finish()
                        } else showToast(apiResponse.error.description)
                    }

                    override fun onApiError(error: String) {
                        dismissProgressBar()
                        Toast.makeText(this@Login, error, Toast.LENGTH_SHORT).show()
                    }
                })
            } else showToast("Invalid Credentials")

        }
    }
}
