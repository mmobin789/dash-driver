package com.webdealer.dashdriver.ui.activities

import android.content.Intent
import android.os.Bundle
import android.support.v4.view.ViewPager
import android.util.Log
import android.view.View
import com.webdealer.dashdriver.R
import com.webdealer.dashdriver.models.DriverBackgroundInfo
import com.webdealer.dashdriver.ui.adapters.SignUpPager
import com.webdealer.dashdriver.ui.fragments.BackgroundCheck
import com.webdealer.dashdriver.ui.fragments.BaseUI
import com.webdealer.dashdriver.ui.fragments.VehicleInformation
import com.webdealer.dashdriver.utils.AppStorage
import kotlinx.android.synthetic.main.activity_verification.*
import kotlinx.android.synthetic.main.toolbar.*
import java.io.File

class Verification : Base(), ViewPager.OnPageChangeListener {

    var phoneNo = ""
    var name = ""
    var email = ""
    var license: File? = null
    var vehicleBook: File? = null
    var carPicture: File? = null
    var insuranceDoc: File? = null
    var profilePicture: File? = null
    var carType = "DashGo"
    var phoneVerified = false
    private var baseUI: BaseUI? = null
    val driverBackgroundInfo = DriverBackgroundInfo()
    private lateinit var adapter: SignUpPager
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_verification)
        init()
    }

    private fun setPageNo(pageNo: Int) {
        Log.i("PageNo", page.toString())
        pager.currentItem = pageNo

    }

    private fun init() {

        setToolbarTitle("Phone Number Verification")
        //      pager.offscreenPageLimit = 9
        adapter = SignUpPager(supportFragmentManager)
        val user = AppStorage.getUser()
        phoneNo = user.phone
        name = user.name
        email = user.email


        // password = intent.getStringExtra("password")

        pager.adapter = adapter
        back.visibility = View.VISIBLE
        forward.visibility = View.VISIBLE

        back.setOnClickListener {
            onBackPressed()
        }
        forward.setOnClickListener {
            onForwardPressed()
        }
        pager.addOnPageChangeListener(this)


    }


    override fun onPageScrollStateChanged(state: Int) {

    }

    override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {

    }

    override fun onPageSelected(position: Int) {

        baseUI = adapter.getItem(position)

        val header = when (position) {

            0 -> "Phone Number Verification"
            1 -> "Background Check"
            2 -> "Vehicle Information"
            3 -> "Profile Photo"
            4 -> "Driving License"
            5 -> "Registration Book"
            6 -> "Insurance Document"
            7 -> "Photo of Car Tag"
            else -> "Welcome"

        }
        setToolbarTitle(header)

        forward.visibility = if (baseUI is BackgroundCheck || baseUI is VehicleInformation)
            View.GONE
        else View.VISIBLE
    }


    override fun onBackPressed() {
//        page = when (pager.currentItem) {
//            0 -> {
//                AppStorage.clearSession()
//                val login = Intent(this, Login::class.java)
//                login.flags = Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT
//                startActivity(login)
//                super.onBackPressed()
//                0
//            }
//            1 -> 0
//            2 -> 1
//            3 -> 2
//            4 -> 3
//            5 -> 4
//            6 -> 5
//            7 -> 6
//            else -> {
//                forward.visibility = View.VISIBLE
//                7
//
//            }
//        }
        page--

        when (page) {
            -1 -> {
                AppStorage.clearSession()
                val login = Intent(this, Login::class.java)
                login.flags = Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT
                startActivity(login)
                super.onBackPressed()
            }
            8 -> forward.visibility = View.VISIBLE
        }


        setPageNo(page)
    }

    private var page = 0

    fun onForwardPressed() {

        page++

        if (page == 8) {
            forward.visibility = View.GONE
        }
        setPageNo(page)
    }

}
