package com.webdealer.dashdriver.ui.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.vansuita.pickimage.listeners.IPickResult
import com.webdealer.dashdriver.R
import com.webdealer.dashdriver.ui.activities.Base.Companion.loadWithGlide
import com.webdealer.dashdriver.ui.activities.Base.Companion.pickImage
import com.webdealer.dashdriver.ui.activities.Verification
import kotlinx.android.synthetic.main.fragment_insurance.*
import java.io.File

class InsuranceDocument : BaseUI() {


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_insurance, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        val verification = base as Verification
        takePic.setOnClickListener { v ->
            pickImage(verification, IPickResult {
                verification.insuranceDoc = File(it.path)
                if (it != null && it.path != null) {
                    loadWithGlide(it.path, photo, false)

                } else showToast("Insurance Document Required")
            })
        }


    }

    companion object {
        fun newInstance() = InsuranceDocument()
    }
}