package com.webdealer.dashdriver.ui.fragments

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.webdealer.dashdriver.R
import com.webdealer.dashdriver.ui.activities.ChangePassword
import com.webdealer.dashdriver.ui.activities.Login
import com.webdealer.dashdriver.ui.activities.MainActivity
import com.webdealer.dashdriver.utils.AppStorage
import kotlinx.android.synthetic.main.fragment_settings.*

class Settings : BaseUI() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_settings, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        changePassword.setOnClickListener {
            startActivity(Intent(it.context, ChangePassword::class.java))
        }
        signOut.setOnClickListener {
            AppStorage.clearSession()

            if (base is MainActivity) {
                val main = base as MainActivity
                if (main.driverLocationSync != null)
                    base.stopService(main.driverLocationSync)
            }
            base.finish()
            val login = Intent(it.context, Login::class.java)
            login.flags = Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT
            startActivity(login)
        }
    }

    companion object {


        fun newInstance() = Settings()
    }
}