package com.webdealer.dashdriver.ui.fragments

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.webdealer.dashdriver.R
import com.webdealer.dashdriver.ui.activities.Verification
import kotlinx.android.synthetic.main.fragment_verification_info.*

class VehicleInformation : BaseUI() {

    private lateinit var verification: Verification
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_verification_info, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        verification = base as Verification

        cont.setOnClickListener {
            getViewData()
            if (verification.driverBackgroundInfo.isValid()) {
                Log.i("driverInfo", verification.driverBackgroundInfo.toJSON())
                verification.onForwardPressed()
            } else showToast("Fields Required")

        }


    }

    private fun getViewData() {
        verification.driverBackgroundInfo.year = etYear.text.toString()
        verification.driverBackgroundInfo.carMake = etMake.text.toString()
        verification.driverBackgroundInfo.vehicleModel = etModel.text.toString()
        verification.driverBackgroundInfo.noOfDoors = etDoors.text.toString()
        verification.driverBackgroundInfo.seatBelts = etS.text.toString()
        verification.driverBackgroundInfo.vehicleColor = etColor.text.toString()

    }


    companion object {
        fun newInstance() = VehicleInformation()
    }
}