package com.webdealer.dashdriver.ui.activities

import android.content.Intent
import android.os.Bundle
import com.webdealer.dashdriver.R
import com.webdealer.dashdriver.contracts.OnApiListener
import com.webdealer.dashdriver.models.ApiResponse
import com.webdealer.dashdriver.models.Bill
import kotlinx.android.synthetic.main.activity_end_ride_payment.*

class EndRidePayment : Base() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_end_ride_payment)
        init()
    }

    private fun init() {
        setToolbarTitle("End Ride")
        payment.setOnClickListener {
            showProgressBar()
            dashDriverImpl.rideComplete(this, object : OnApiListener {
                override fun onApiResponse(apiResponse: ApiResponse) {
                    dismissProgressBar()
                    showToast(if (apiResponse.status) {
                        startActivity(Intent(it.context, RateRider::class.java))
                        "Payment Received"

                    } else apiResponse.error.description)

                }

                override fun onApiError(error: String) {
                    dismissProgressBar()
                    showToast(error)
                }
            })

        }
        val bill = intent.getParcelableExtra<Bill>("bill")
        priceTV.text = bill.total
        startingPrice.text = bill.tripFare
        movingPrice.text = bill.tolls
        waitingPrice.text = bill.discount
        //  serviceFee.text =


    }
}
