package com.webdealer.dashdriver.ui.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.webdealer.dashdriver.R
import com.webdealer.dashdriver.ui.activities.Verification
import kotlinx.android.synthetic.main.verify_phone.*

class VerifyPhone : BaseUI() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.verify_phone, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val verification = base as Verification
        proceed.setOnClickListener {
            val phone = etPhone.text.toString()
            if (phone.isBlank())
                showToast("Phone Required")
            else {

                val fullNumber = ccp.selectedCountryCodeWithPlus + phone
                if (fullNumber == verification.phoneNo)
                //  verification.setPageNo(1)
                else showToast("Phone Number Mismatched.")
            }
        }
    }

    companion object {
        fun newInstance() = VerifyPhone()
    }

}