package com.webdealer.dashdriver.contracts

interface OnApiErrorListener {
    fun onApiError(error: String)
}