package com.webdealer.dashdriver.contracts

import com.webdealer.dashdriver.models.ApiResponse

interface OnApiListener : OnApiErrorListener {
    fun onApiResponse(apiResponse: ApiResponse)
}