package com.webdealer.dashdriver.contracts

interface OnInstagramAccessListener {
    fun onInstagramAccessGranted(accessToken: String)
}