package com.webdealer.dashdriver.contracts

import com.webdealer.dashdriver.api.Instagram

interface OnInstagramLoginListener {
    fun onSuccess(instagram: Instagram)
}