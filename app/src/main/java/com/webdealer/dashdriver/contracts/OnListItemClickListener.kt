package com.webdealer.dashdriver.contracts

interface OnListItemClickListener {
    fun onListItemClicked(position: Int)
}