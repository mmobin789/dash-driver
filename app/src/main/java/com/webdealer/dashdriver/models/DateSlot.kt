package com.webdealer.dashdriver.models

class DateSlot(val month: String, val dayOfWeek: String, val dayOfMonth: Int, var isSet: Boolean)