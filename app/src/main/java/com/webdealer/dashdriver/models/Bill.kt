package com.webdealer.dashdriver.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Bill(val total: String, val tripFare: String, val discount: String, val tolls: String) : Parcelable