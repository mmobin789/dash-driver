package com.webdealer.dashdriver.models

enum class HomeNavigation {
    RideOrDelivery, Home, ConfirmArrival, StartRide, EndRide
}