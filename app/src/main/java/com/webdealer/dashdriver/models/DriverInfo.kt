package com.webdealer.dashdriver.models

import com.webdealer.dashdriver.utils.AppStorage

data class DriverInfo(val lastTripEarnings: String, val lastTripMiles: String) {
    val isRider = "0"
    var rideID = "0"
    val email = AppStorage.getUser().email
    var currentlat = ""
    var currentlng = ""
}