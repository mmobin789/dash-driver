package com.webdealer.dashdriver.models

import com.google.gson.Gson

class DriverBackgroundInfo {
    var uber = false
    var lyft = false
    var firstName = ""
    var lastName = ""
    var middleName = ""
    var socialSecurityNo = ""
    var licenseCardNo = ""
    var uberRating: Float? = null
    var lyftRating: Float? = null
    var dob = ""
    var expiryDate = ""
    var state = ""
    var city = ""
    var zipCode = ""
    var address = ""
    var year = ""
    var vehicleModel = ""
    var carMake = ""
    var noOfDoors = ""
    var vehicleColor = ""
    var seatBelts = ""

    fun isValid() = firstName.isNotBlank() && lastName.isNotBlank() && middleName.isNotBlank() && socialSecurityNo.isNotBlank()
            && licenseCardNo.isNotBlank() && dob.isNotBlank() && expiryDate.isNotBlank() && state.isNotBlank() && city.isNotBlank()
            && zipCode.isNotBlank() && address.isNotBlank() && year.isNotBlank() && vehicleModel.isNotBlank() && carMake.isNotBlank()
            && noOfDoors.isNotBlank() && vehicleColor.isNotBlank() && seatBelts.isNotBlank()


    fun isBackGroundValid() = firstName.isNotBlank() && lastName.isNotBlank() && middleName.isNotBlank() && socialSecurityNo.isNotBlank()
            && licenseCardNo.isNotBlank() && dob.isNotBlank() && expiryDate.isNotBlank() && state.isNotBlank() && city.isNotBlank()
            && zipCode.isNotBlank() && address.isNotBlank()

    fun toJSON() = Gson().toJson(this)

}