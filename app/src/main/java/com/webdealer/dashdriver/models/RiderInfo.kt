package com.webdealer.dashdriver.models

data class RiderInfo(val name: String, val phone: String, val email: String,
                     val carType: String, val picklat: String, val picklng: String, val droplat: String?, val droplng: String?)