package com.webdealer.dashdriver.models

data class User(val name: String, val email: String,
                val phone: String, var status: String?)