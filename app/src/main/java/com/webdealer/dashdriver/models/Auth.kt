package com.webdealer.dashdriver.models

import com.webdealer.dashdriver.utils.AppStorage
import com.webdealer.dashdriver.utils.Utils

data class Auth(val email: String, val password: String) {
    var name = ""
    var phone = ""
    var code = ""
    val type = "1"
    var rideID = "-1"
    var driverStatus = "1"
    val fcmToken = AppStorage.getFCM()


    constructor(name: String, email: String, phone: String, password: String) : this(email, password) {
        this.name = name
        this.phone = phone

    }

    fun isValid() = name.isNotBlank() && phone.isNotBlank() && Utils.isValidEmail(email) && password.isNotBlank()
}