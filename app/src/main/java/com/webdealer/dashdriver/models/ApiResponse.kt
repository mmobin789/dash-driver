package com.webdealer.dashdriver.models

data class ApiResponse(val status: Boolean, val data: Data, val error: Error, val message: String, val driverInfo: DriverInfo, val bill: Bill, val driverStatus: String
                       , val rideInfo: RiderInfo
) {
    data class Data(val user: User)
    data class Error(val code: Int, val description: String)
}